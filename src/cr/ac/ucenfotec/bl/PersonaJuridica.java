package cr.ac.ucenfotec.bl;

public class PersonaJuridica extends Persona{

    private String representante;
    private String industria;

    public PersonaJuridica() {
    }

    public PersonaJuridica(String identificacion, String nombre, String direccion,
                           String telefono, String representante, String industria) {
        super(identificacion, nombre, direccion, telefono);
        this.representante = representante;
        this.industria = industria;
    }

    public String getRepresentante() {
        return representante;
    }

    public void setRepresentante(String representante) {
        this.representante = representante;
    }

    public String getIndustria() {
        return industria;
    }

    public void setIndustria(String industria) {
        this.industria = industria;
    }

    public String toString() {
        return super.toString() + ", representante=" + representante + ", industria=" + industria;
    }
}
