package cr.ac.ucenfotec.bl;

import java.time.LocalDate;

public class PersonaFisica extends Persona{
    private String apellido;
    private char genero;
    private int edad;
    private LocalDate fechaNacimiento;

    public PersonaFisica() {
    }

    public PersonaFisica(String identificacion, String nombre, String direccion,
                         String telefono, String apellido, char genero, int edad,
                         LocalDate fechaNacimiento) {
        super(identificacion, nombre, direccion, telefono);
        this.apellido = apellido;
        this.genero = genero;
        this.edad = edad;
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String toString() {
        return super.toString() + ", apellido=" + apellido +", genero=" + genero +
                ", edad=" + edad +", fechaNacimiento=" + fechaNacimiento;
    }
}