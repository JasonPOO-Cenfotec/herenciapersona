package cr.ac.ucenfotec.ui;

import cr.ac.ucenfotec.bl.Persona;
import cr.ac.ucenfotec.bl.PersonaFisica;
import cr.ac.ucenfotec.bl.PersonaJuridica;

import java.time.LocalDate;

public class UI {
    public static void main(String[] args) {
        Persona persona1 = new Persona("123","Juan","Alajuela","88888888");
        PersonaJuridica personaJuridica1 = new PersonaJuridica("3-101-10101","La arboleda","Heredia","22525552","Ana Morales","Embutidos");
        PersonaFisica personaFisica1 = new PersonaFisica("456","Carlos","Puntarenas","9999999","Pérez",'M',52, LocalDate.of(1970,01,05));

        System.out.println("Persona:" + persona1);
        System.out.println("PersonaJuridica: " + personaJuridica1);
        System.out.println("PersonaFisica: " + personaFisica1);
    }
}
